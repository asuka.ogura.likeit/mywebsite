package todo;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.TodoDao;
import dto.TodoDto;
import dto.UserDto;

/**
 * TODO一覧画面
 * 
 * @author a-ogura
 */
@WebServlet("/ListServlet")
public class ListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      HttpSession session = request.getSession();

      /**** ログインの有無確認 start ****/
      UserDto loginUser = (UserDto) session.getAttribute("userInfo");


      // ログイン情報がない場合
      if (loginUser == null) {
        // セッションがなかったらログイン画面へ
        response.sendRedirect("LoginServlet");
        return;

      }
      /**** ログインの有無確認 end ****/


      // TODO一覧情報を取得
      TodoDao todoDao = new TodoDao();
      List<TodoDto> todoList = todoDao.findAll(loginUser.getId());

      // リクエストスコープにTODO一覧情報をセット
      request.setAttribute("todoList", todoList);

      // TODO一覧のjspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher(TodoHelper.LIST_PAGE);
      dispatcher.forward(request, response);
	}

}
