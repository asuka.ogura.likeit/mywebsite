package todo;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.TodoDao;
import dto.UserDto;

/**
 * TODO追加画面
 * 
 * @author a-ogura
 *
 */

/**
 * Servlet implementation class CreateServlet
 */
@WebServlet("/CreateServlet")
public class CreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      HttpSession session = request.getSession();

      /**** ログインの有無確認 start ****/
      // ログインユーザ情報を取得
      UserDto loginUser = (UserDto) session.getAttribute("userInfo");

      /* ユーザ情報が取得できない場合 */
      if (loginUser == null) {
        response.sendRedirect("LoginServlet");
        return;

      }
      /**** ログインの有無確認 end ****/

      // 新規登録のjspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher(TodoHelper.CREATE_PAGE);
      dispatcher.forward(request, response);


	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      // ログインユーザのIDを取得
	  HttpSession session = request.getSession();
      UserDto loginUser = (UserDto) session.getAttribute("userInfo");

      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // 新規登録画面で入力された情報を取得
      String title = request.getParameter("title");
      
      TodoDao todoDao = new TodoDao();

      // 例外処理：未入力時
      if (title.equals("")) {

        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力した内容は正しくありません");

        // // リクエストスコープに入力項目をセット
        // request.setAttribute("title", title);


        // フォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher(TodoHelper.CREATE_PAGE);
        dispatcher.forward(request, response);
        return;

      } else {
        /**** 新規登録成功時 ****/

        // ユーザー情報の登録
        todoDao.insertTodo(title, loginUser.getId());

        // ユーザ一覧のサーブレットにリダイレクト
        response.sendRedirect("ListServlet");

      }

    }

}
