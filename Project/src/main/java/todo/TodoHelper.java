package todo;

import javax.servlet.http.HttpSession;

/**
 * 処理及び表示簡略化ヘルパークラス
 *
 * @author a-ogura
 *
 */

public class TodoHelper {

  // ログイン
  static final String LOGIN_PAGE = "/WEB-INF/jsp/login.jsp";
  // TODO一覧
  static final String LIST_PAGE = "/WEB-INF/jsp/list.jsp";
  // TODO登録
  static final String CREATE_PAGE = "/WEB-INF/jsp/create.jsp";
  // TODO更新
  static final String UPDATE_PAGE = "/WEB-INF/jsp/update.jsp";

  /**
   * セッションから指定データを取得（削除も一緒に行う）
   *
   * @param session
   * @param str
   * @return
   */
  public static Object cutSessionAttribute(HttpSession session, String str) {
    Object test = session.getAttribute(str);
    session.removeAttribute(str);

    return test;
  }


}
