package todo;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.TodoDao;
import dto.TodoDto;
import dto.UserDto;

/**
 * Servlet implementation class SearchServlet
 */
@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      // ログイン情報取得
      HttpSession session = request.getSession();
      UserDto loginUser = (UserDto) session.getAttribute("userInfo");


      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // インスタンス化
      TodoDao todoDao = new TodoDao();

      // 検索条件取得
      String title = request.getParameter("title");

      // 検索処理
      List<TodoDto> todoList = todoDao.search(loginUser.getId(), title);

      // リクエストスコープにユーザ一覧情報をセット
      request.setAttribute("todoList", todoList);

      // ユーザ一覧のjspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher(TodoHelper.LIST_PAGE);
      dispatcher.forward(request, response);



	}

}
