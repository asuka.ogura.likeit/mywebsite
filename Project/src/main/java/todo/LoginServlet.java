package todo;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import dto.UserDto;
import util.PasswordEncorder;

/**
 * ログイン画面
 * 
 * @author a-ogura
 *
 */

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession();

    /**** ログインの有無確認 start ****/
    // ログイン情報を取得
    UserDao loginUser = (UserDao) session.getAttribute("userInfo");

    // ログイン情報があればユーザー一覧へ遷移
    if (loginUser != null) {
      response.sendRedirect("ListServlet");
      return;

    }
    /**** ログインの有無確認 end ****/

    // フォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher(TodoHelper.LOGIN_PAGE);
    dispatcher.forward(request, response);

  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータの入力項目を取得
    String email = request.getParameter("email");
    String password = request.getParameter("password");

    // パスワードの暗号化
    PasswordEncorder pEncorder = new PasswordEncorder();
    // パスワード(暗号化後)とログインIDでユーザー情報を検索
    String encryptPass = pEncorder.encordPassword(password);

    // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
    UserDao userDao = new UserDao();
    UserDto user = userDao.findByLoginInfo(email, encryptPass);


    /** テーブルに該当のデータが見つからなかった場合 * */
    if (user == null) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("errMsg", "メールアドレスまたはパスワードが異なります");
      // 入力したログインIDを画面に表示するため、リクエストに値をセット
      request.setAttribute("email", email);

      // ログインjspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher(TodoHelper.LOGIN_PAGE);
      dispatcher.forward(request, response);
      return;
    }

    /** テーブルに該当のデータが見つかった場合 * */
    // セッションにユーザの情報をセット
    HttpSession session = request.getSession();
    session.setAttribute("userInfo", user);

    // ユーザ一覧のサーブレットにリダイレクト
    response.sendRedirect("ListServlet");


  }



}
