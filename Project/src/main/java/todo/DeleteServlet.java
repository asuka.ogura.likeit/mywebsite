package todo;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.TodoDao;
import dto.UserDto;

/**
 * TODO削除削除機能
 * 
 * @author a-ogura
 *
 */

/**
 * Servlet implementation class DeleteServlet
 */
@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

      HttpSession session = request.getSession();

      /**** ログインの有無確認 start ****/
      UserDto loginUser = (UserDto) session.getAttribute("userInfo");

      /* ユーザ情報が取得できない場合 */
      if (loginUser == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      /**** ログインの有無確認 end ****/


      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      int todoId = Integer.valueOf(request.getParameter("id"));

      // TODO削除
      TodoDao todoDao = new TodoDao();
      todoDao.deleteTodo(todoId);

      // ユーザ一覧のサーブレットにリダイレクト
      response.sendRedirect("ListServlet");

    }


}
