package todo;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.TodoDao;
import dto.TodoDto;
import dto.UserDto;

/**
 * TODO更新画面
 * 
 * @author a-ogura
 *
 */

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession();

    /**** ログインの有無確認 start ****/
    UserDto loginUser = (UserDto) session.getAttribute("userInfo");

    /* ユーザ情報が取得できない場合 */
    if (loginUser == null) {
      response.sendRedirect("LoginServlet");
      return;
    }
    /**** ログインの有無確認 end ****/

    /* ユーザ情報が取得できた場合 */
    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータの値を取得
    int todoId = Integer.valueOf(request.getParameter("id"));

    // リクエストパラメータで取得した値を引数に、findByIdメソッドを実行
    TodoDao todoDao = new TodoDao();
    TodoDto todo = todoDao.findByTodoId(todoId);

    // findByIdメソッドの戻り値をリクエストスコープにセット
    request.setAttribute("todo", todo);
    request.setAttribute("title", todo.getTitle());

    /* userUpdate.jspへフォワード */
    RequestDispatcher dispatcher = request.getRequestDispatcher(TodoHelper.UPDATE_PAGE);
    dispatcher.forward(request, response);



  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータの入力項目を取得
    int todoId = Integer.valueOf(request.getParameter("todo-id"));
    int userId = Integer.valueOf(request.getParameter("user-id"));
    String title = request.getParameter("title");

    // インスタンス化
    TodoDao todoDao = new TodoDao();


    // 例外処理
    if (title.equals("")) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("errMsg", "入力した内容は正しくありません");

      // リクエストスコープに入力項目をセット
      request.setAttribute("title", title);

      // フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher(TodoHelper.UPDATE_PAGE);
      dispatcher.forward(request, response);
      return;

    } else {

      // TODOの更新
      todoDao.updateTodo(title, todoId);

      // リダイレクト
      response.sendRedirect("ListServlet");

    }
  }

}
