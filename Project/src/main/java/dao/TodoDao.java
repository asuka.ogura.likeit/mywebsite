package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import base.DBManager;
import dto.TodoDto;

/**
 * TODOテーブル用のDao
 *
 * 
 */

public class TodoDao {

  /**
   * ログインユーザの全TODO情報を取得する
   *
   * @return ログインユーザの全TODO情報一覧
   */
  public List<TodoDto> findAll(int userId) {
    Connection conn = null;
    List<TodoDto> todoList = new ArrayList<TodoDto>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM t_todo WHERE user_id = ? ORDER BY id DESC";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, userId);
      ResultSet rs = pStmt.executeQuery();

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String title = rs.getString("title");
        String user_id = rs.getString("user_id");
        TodoDto todo = new TodoDto(id, title, user_id);

        todoList.add(todo);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return todoList;
  }

  /**
   * TODO追加
   *
   * @Param title
   * @param userId
   * @return なし
   */
  public void insertTodo(String title, int userId) {


    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      // INSERT文を準備
      String sql = "INSERT INTO t_todo (title, user_id) VALUES (?, ?)";
      // INSERTを実行
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, title);
      pStmt.setInt(2, userId);
      pStmt.executeUpdate();
      pStmt.close();
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

  }

  /**
   * TODO更新
   * 
   * @param title
   * @param todoId
   * @return なし
   */

  public void updateTodo(String title, int todoId) {

    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      // SQL準備
      String sql = "UPDATE t_todo SET title = ? WHERE id = ?";
      // SQL実行
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, title);
      pStmt.setInt(2, todoId);

      pStmt.executeUpdate();
      pStmt.close();
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

  }

  /**
   * TODO削除
   *
   * @Param todoId
   * @return なし
   */
  public void deleteTodo(int todoId) {
    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      // SQL準備
      String sql = "DELETE FROM t_todo WHERE id = ?;";
      // SQL実行
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, todoId);

      pStmt.executeUpdate();
      pStmt.close();
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

  }

  /**
   * DBのIDに紐づくTODO情報を返す
   * 
   * @param todoId
   * @return 該当のTODO情報
   */
  public TodoDto findByTodoId(int todoId) {

    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM t_todo WHERE id = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, todoId);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String title = rs.getString("title");
      String user_id = rs.getString("user_id");
      return new TodoDto(id, title, user_id);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }



  }

  /**
   * 検索に一致する全TODO情報を取得
   * 
   * @param userId
   * @param title
   * @return 該当したTODOリスト
   */
  public List<TodoDto> search(int userId, String title) {

    Connection conn = null;
    List<TodoDto> todoList = new ArrayList<TodoDto>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SQL準備
      String sql = "SELECT * FROM t_todo WHERE user_id = ? AND title LIKE ? ORDER BY id DESC";

      // SQL実行
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, userId);
      pStmt.setString(2, "%" + title + "%");
      ResultSet rs = pStmt.executeQuery();

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String _title = rs.getString("title");
        String user_id = rs.getString("user_id");
        TodoDto todo = new TodoDto(id, _title, user_id);

        todoList.add(todo);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return todoList;


  }

}
