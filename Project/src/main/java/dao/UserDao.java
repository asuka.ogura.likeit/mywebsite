package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import base.DBManager;
import dto.UserDto;

/**
 * ユーザテーブル用のDao
 *
 * 
 */

public class UserDao {



  /**
   * メールアドレスとパスワードに紐づくユーザ情報を返す
   * 
   * @param email
   * @param password
   * @return
   */

  public UserDto findByLoginInfo(String email, String password) {
    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM t_user WHERE login_id = ? and login_password = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, email);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String name = rs.getString("name");
      String _email = rs.getString("login_id");// 変数名がメソッドの引数と同じになってしまうため、別の名前を設定
      String _password = rs.getString("login_password"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定
      return new UserDto(id, name, _email, _password);

    } catch (SQLException e) {
      e.printStackTrace();
      System.out.println(e.getMessage());

      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          System.out.println(e.getMessage());
          return null;
        }
      }
    }
  }



}
