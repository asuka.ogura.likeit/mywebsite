package util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Scanner;
import javax.xml.bind.DatatypeConverter;

public class PasswordEncorder {

  public String encordPassword(String password) {
    // インスタンス化
    Scanner scan = new Scanner(System.in);

    // ハッシュ生成前にバイト配列に置き換える際のCharset
    Charset charset = StandardCharsets.UTF_8;

    // ハッシュ関数の種類（MD5）
    String algorithm = "MD5";

    // ハッシュ生成処理
    try {
      byte[] bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));

      // ハッシュ配列を文字列化
      String encryptPass = DatatypeConverter.printHexBinary(bytes);

      // 戻り値として返す
      return encryptPass;

    } catch (Exception e) {

      e.printStackTrace();
      return null;

    }

  }

}
