package dto;

/**
 * Userテーブルのデータを格納するためのDto
 * 
 * @author a-ogura
 * 
 **/

public class UserDto {

  private int id;
  private String name;
  private String email;
  private String login_password;

  // 全てのデータをセットするコンストラクタ
  public UserDto(int id, String name, String email, String login_password) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.login_password = login_password;
  }


  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String login_id) {
    this.email = email;
  }

  public String getLogin_password() {
    return login_password;
  }

  public void setLogin_password(String login_password) {
    this.login_password = login_password;
  }


}
