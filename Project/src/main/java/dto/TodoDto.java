package dto;

/**
 * Todoテーブルのデータを格納するためのDto
 * 
 * @author a-ogura
 * 
 **/

public class TodoDto {

  private int id;
  private String title;
  private String user_id;

  // 全てのデータをセットするコンストラクタ
  public TodoDto(int id, String title, String user_id) {
    this.id = id;
    this.title = title;
    this.user_id = user_id;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }



}
