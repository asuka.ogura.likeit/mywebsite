﻿--Databese: 'todo_db'
CREATE DATABASE todo_db DEFAULT CHARACTER SET utf8;

-- todo_dbを指定するSQL
USE todo_db;

-- テーブルの構造 't_user'
CREATE TABLE t_user (
id int PRIMARY KEY AUTO_INCREMENT,
name varchar(255) NOT NULL,
login_id varchar(255) UNIQUE NOT NULL,
login_password varchar(255) NOT NULL
);

-- テーブルデータの追加 't_user'
INSERT INTO t_user(name,login_id,login_password) VALUES(
'一般01',
'user01',
'password'
);

-- テーブルの構造 't_todo'
CREATE TABLE t_todo(
id SERIAL PRIMARY KEY AUTO_INCREMENT,
title varchar(255) NOT NULL,
user_id int NOT NULL,
FOREIGN KEY fk_user_id(user_id) REFERENCES t_user(id)
);




